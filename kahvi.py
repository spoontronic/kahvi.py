#!/user/bin/python3
# Python Script to Download, Unzip and Play Kahvi Sounds

import subprocess
import requests
import getpass
import glob
import re
import os
from zipfile import ZipFile
from urllib import request
from tqdm import tqdm
from typing import List, Optional


# download progress bar
class DownloadProgress(tqdm):
    def show_progress(
        self, b: int = 1, bsize: int = 1, tsize: Optional[int] = None
    ) -> None:
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


# download url and show progress
def download_url(url: str, output_path: str) -> None:
    with DownloadProgress(
        unit="B", unit_scale=True, miniters=1, desc=url.split("/")[-1], colour="green"
    ) as t:
        request.urlretrieve(url, filename=output_path, reporthook=t.show_progress)


# funny welcome
user = getpass.getuser()
try:
    subprocess.call(
        [
            "cowsay",
            "-f",
            "turtle",
            "Welcome",
            user[0].upper() + user[1:] + ", \n let's get some marvelous Kahvi sounds!",
        ]
    )
except FileNotFoundError:
    print(
        "Welcome! \nNote: The script uses cowsay, please install the package on your system."
    )
print("")

# interaction with user in the beginning
while 1:
    dir = input("Please enter a storage location: \n").strip()
    # add slash to location if not specified
    if dir.endswith("/"):
        pass
    else:
        dir = dir + "/"
    try:
        os.chdir(os.path.abspath(dir))
        print("")
        break

    except FileNotFoundError:
        print("Error: The Storage location does not exist. \n")

while 1:
    try:
        x = input("With which release number this script should start? \n")
        y = input("With which release number this script should end? \n")

        r = [int(x), int(y)]  # list w/ release numbers
        n = abs(int(y) - int(x)) + 1  # number of releases

        # introducing summary
        if n == 1:
            print(f"All right, this is only {n} release. Here we go: \n")
        if n >= 2:
            print(f"All right, these are {n} releases. Here we go: \n")

        break

    except ValueError:
        print("Invalid: Please enter only three-digit numbers. \n")


def add_zeros(release_number: int) -> str:
    """
    add left-hand zeros to release number
    """
    return "{:03d}".format(release_number)


def get_kahvi() -> None:
    """
    main function of this script to get the Kahvi sounds
    """
    counter = 0
    new_albums = []
    success = []
    failure = []

    for release_number in range(min(r), max(r) + 1):
        url = (
            f"http://kahvi.org/releases.php?release_number={add_zeros(release_number)}"
        )
        htmltext = requests.get(url).text

        # grep download link of zip archive from html source (ogg preferred)
        if re.findall(
            r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*ogg.*zip", htmltext
        ):
            zip_link = re.findall(
                r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*ogg.*zip",
                htmltext,
            )
            audio_format = "ogg"
        else:
            zip_link = re.findall(
                r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*mp3.*zip",
                htmltext,
            )
            audio_format = "mp3"

        counter += 1

        try:
            # parse album name
            album_name = zip_link[0].rsplit("/", 1)[1].rsplit("_", 1)[0]

            # check if the release already exists
            if os.path.exists(album_name):
                print(
                    f"({counter}/{n}) Skipped: The release #{release_number} already exists. \n"
                )
            else:
                try:
                    # download archives
                    print(
                        f"({counter}/{n}) Downloading zip archive of Kahvi release #{release_number}..."
                    )
                    download_url(str(zip_link[0]), album_name + ".zip")
                    # request.urlretrieve(str(zip_link[0]), album_name + ".zip")
                    print("Done! \n")

                    # extract downloaded zip files
                    with ZipFile(album_name + ".zip", "r") as zip:
                        print("Content of this zip archive:")
                        zip.printdir()
                        print("Extracting all these files now...")
                        zip.extractall(path=dir + album_name)
                        print("Done! \n")

                    # list of new added albums
                    new_albums.append(album_name)
                    success.append("#" + str(release_number))

                except Exception as e:
                    print(f"Error: No such file: {str(zip_link[0])} \n")
                    # print(e)
                    failure.append("#" + str(release_number))
                    continue

        except IndexError:
            print(
                f"({counter}/{n}) Error: Could not find download link of a zip archive for release #{release_number} "
                f"at {url}.\nTrying something other...\n"
            )
            failure.append("#" + str(release_number))

            # grep download link of single file from html source (ogg preferred)
            if re.findall(
                r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*ogg", htmltext
            ):
                track_link = re.findall(
                    r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*ogg",
                    htmltext,
                )
                audio_format = "ogg"
            else:
                track_link = re.findall(
                    r"ftp://ftp.scene.org/pub/music/groups/kahvicollective/.*mp3",
                    htmltext,
                )
                audio_format = "mp3"

            try:
                # parse album & file name
                album_name = track_link[0].rsplit("/", 1)[1].rsplit(".", 1)[0]
                file_name = track_link[0].rsplit("/", 1)[1]

                # check if the release already exists
                if os.path.exists(album_name):
                    print(f"Skipped: The release #{release_number} already exists. \n")

                    # remove album from error list
                    failure.remove("#" + str(release_number))

                else:
                    try:
                        # download file
                        print(
                            f"Note: Kahvi release #{release_number} consists of only one track ({file_name}). \n"
                            f"Download is running..."
                        )
                        os.mkdir(album_name)
                        request.urlretrieve(
                            str(track_link[0]),
                            album_name + "/" + track_link[0].rsplit("/", 1)[1],
                        )
                        print("Done! \n")

                        # list of new added albums
                        new_albums.append(album_name)
                        success.append("#" + str(release_number))

                        # remove album from error list
                        failure.remove("#" + str(release_number))

                    except Exception as e:
                        print(f"Error: No such file: {str(track_link[0])} \n")
                        # print(e)
                        continue

            except IndexError:
                print(
                    f"Error: Could even not find download link of a single file for release "
                    f"#{release_number} at {url}. \n"
                )

    closing_summary(new_albums, success, failure)
    remove_files()
    play_kahvi(new_albums, audio_format)


def closing_summary(
    new_albums: List[str], success: List[str], failure: List[str]
) -> None:
    """
    output of a short closing summary
    """
    print("Summary:")

    if n == 1:
        print(f"Downloaded {len(new_albums)} of {n} release.")
    if n >= 2:
        print(f"Downloaded {len(new_albums)} of {n} releases.")

    if len(success) == 1:
        print(f"New is the release {success[0]}.")
    if len(success) == 2:
        print(
            f"New are the releases {' and '.join(success)}."
        )  # two release numbers of list success
    if len(success) > 2:
        print(
            f"There are {len(success)} new releases: {', '.join(success)}."
        )  # comma separated release numbers of list success

    if len(failure) == 1:
        print(f"An error occurred with release {failure[0]}.")
    if len(failure) == 2:
        print(
            f"Errors occurred with the releases {' and '.join(failure)}."
        )  # two release numbers of list failure
    if len(failure) > 2:
        print(
            f"Errors occurred with {len(failure)} releases: {', '.join(failure)}."
        )  # comma separated release numbers of list failure


def remove_files() -> None:
    """
    deletion of txt files in all subdirectories and all zip archives in the download folder
    """
    print("")
    print("Cleaning directories...")

    for file in glob.glob(f"kahvi*/scene.org.txt"):
        os.remove(file)
        print(f"Deleted: {dir + file}")

    for item in os.listdir(dir):
        if item.endswith(".zip"):
            os.remove(os.path.join(dir, item))
            print(f"Deleted: {dir + item}")

    print("")


def play_kahvi(new_albums: List[str], audio_format: str) -> None:
    """
    offer to play new downloaded tracks w/ ogg123 resp. moc
    Note: MOC has no pulseaudio support, but mpg123 doesn't run w/ new_album-list and mc-style of moc is quiet nice.
    """
    if len(new_albums) >= 1:
        feedback = None
        while feedback not in ("y", "n"):
            feedback = str(
                input("Do you want to play new added tracks? [y/n] ").lower().strip()
            )
            if feedback[:1] == "y":
                if audio_format == "ogg":
                    try:
                        ogg123 = ["ogg123", "-d", "pulse", "-z"]
                        ogg123.extend(new_albums)
                        subprocess.call(ogg123)
                    except FileNotFoundError:
                        print(
                            "Error: The script uses ogg123, please install the package vorbis-tools on your system."
                        )
                if audio_format == "mp3":
                    try:
                        moc = ["mocp"]
                        moc.extend(new_albums)
                        subprocess.call(moc)
                    except FileNotFoundError:
                        print(
                            "Error: The script uses MOC (music on console), please install the package on your system"
                        )
            elif feedback[:1] == "n":
                print("Ok, see you soon!")
            else:
                print('Please enter "y" or "n". ')
            print("")
    else:
        pass


get_kahvi()
